(function( $ ){
  $(document).ready(function(){
    $(".expand-content").hide();
    $("a.expand-title span").addClass("expand");
    $("a.expand-title").toggle(function(){
      var t = $(this).attr("href");
      $(t).slideToggle();
      $("span", this).removeClass("expand");
      $("span", this).addClass("collapse");
      return false;
    }, function() {
      var t = $(this).attr("href");
      $(t).slideToggle();
      $("span", this).removeClass("collapse");
      $("span", this).addClass("expand");
      return false;
    });
  
  	
  });
})( jQuery );